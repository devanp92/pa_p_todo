class Customer < ActiveRecord::Base
  has_many :todos
  validates_formatting_of :phone_num, using: :us_phone
  validates_formatting_of :zip_code, using: :us_zip
  validates_formatting_of :email, using: :email
end
