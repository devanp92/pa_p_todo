class CreateTodos < ActiveRecord::Migration
  def change
    create_table :todos do |t|
      t.references :customer, index: true
      t.text :todo

      t.timestamps
    end
  end
end
