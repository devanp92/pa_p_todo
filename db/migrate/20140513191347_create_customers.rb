class CreateCustomers < ActiveRecord::Migration
  def change
    create_table :customers do |t|
      t.string :first_name
      t.string :last_name
      t.string :phone_num
      t.text :address_1
      t.text :address_2
      t.text :address_3
      t.string :zip_code
      t.text :city
      t.string :email

      t.timestamps
    end
  end
end
